﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// GamePreferences class.
/// Sets the settings for our game.
/// </summary>
public class GamePreferences : MonoBehaviour
{
    public static GamePreferences singleton { get; private set; } //Singleton

    //Ads region
#if UNITY_ANDROID || UNITY_IOS || UNITY_WP8
    [Header ("Ads section")]
    public bool ENABLE_END_OF_LEVEL_UNITY_ADS = true; //enable ads or not
    public int SHOW_ADS_AFTER_NUMBER_OF_GAMES = 3; //number games to show an ad

    public bool ENABLE_CONTINUE_GAME_REWARDED_AD = true; //Would you like to let the player continue the game after he loses by watching a rewarded ad?
#endif

    [Space (20)]
    [Header ("Rate section")]
    //URLs regions
    public bool ENABLE_RATE_BUTTON = true;
    public string URL_ANDROID_GAME = "http://www.google.com"; //link to your android game
    public string URL_IOS_GAME = "http://www.google.com"; //link your your ios game
    public string URL_WP_GAME = "http://www.google.com"; //Link to your windows phone game
    public string URL_WEBGL_GAME = "http://www.google.com"; //link to your webgl game
    public string URL_MAC_GAME = "http://www.google.com"; //link to your mac game
    public string URL_WINDOWS_GAME = "http://www.google.com"; //link to your windows game

    //More games button
    [Space (20)]
    [Header ("More games section")]
    public bool ENABLE_MORE_GAMES_BUTTON = true;
    public string URL_ANDROID_MORE_GAMES = "http://www.google.com"; //Link to your googleplay account
    public string URL_IOS_MORE_GAMES = "http://www.google.com"; //Link to your ios account
    public string URL_WP_MORE_GAMES = "http://www.google.com"; //Link to your windows phone account
    public string URL_WEBGL_MORE_GAMES = "http://www.google.com"; //Link to your web gl games
    public string URL_MAC_MORE_GAMES = "http://www.google.com"; //Link to your mac games
    public string URL_WINDOWS_MORE_GAMES = "http://www.google.com"; //Link to your windows games
    
    //Contact settings
    [Space (20)]
    [Header ("Contact")]
    public bool ENABLE_SETTINGS_CONTACT_SECTION = true; //Enable contact section in the settings panel
    public string SETTINGS_CONTACT_EMAIL = "contact@tornadobandits.com"; //Contact email

    //Enable number of games played
    [Space (20)]
    [Header ("Number of games played section")]
    public bool ENABLE_GAMES_PLAYED_SECTION = true;

    /// <summary>
    /// Awake function.
    /// </summary>
    private void Awake ()
    {
        //We need only one type of this class and it will have to be present in all our scenes
        if (singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad (this.gameObject);
        }
        else
        {
            Debug.LogWarning ("It looks like you have more than one GamePreferences in one scene. Take care because we will delete until them we have only one.");
            DestroyImmediate (this.gameObject);
        }
    }
}
