﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PoolManager that helps us to reuse memory and don't instantiate/destroy too much.
/// </summary>
public class PoolManager : MonoBehaviour
{
    public static PoolManager singleton { get; private set; } //singleton.

    private Dictionary<int, List<Cake>> poolDictionary = new Dictionary<int, List<Cake>> (); //Dictionary containig a key for each Cake and the list of Cakes that might be used.

    public List<Cake> objectsToPool; //this is the list that contains all the objects PREFABS that we want to pool

    /// <summary>
    /// Unity Awake call.
    /// </summary>
    private void Awake ()
    {
        if (singleton == null)
        {
            singleton = this;
        } else
        {
            DestroyImmediate (this);
        }
    }

    /// <summary>
    /// Adds a new pool object to our dictionary.
    /// This is called from GameplayManager or from Destroyable objects that have debris.
    /// </summary>
    /// <param name="newCake">The new to pool object.</param>
    public void AddObjectToPoolDictionary (Cake newCake, int size)
    {
        //Lets make sure that we don't have any pool object with this id.
        int poolKey = newCake.GetInstanceID ();

        if (poolDictionary.ContainsKey (poolKey) == false)
        {
            //If we don't have the id of this pool object in our pool manager 
            //Then just create a new entry for our dictionary
            //We will Instantiate one object and add it to the list
            //You can extend it and spawn like 10 or more objects
            List<Cake> newCakesList = new List<Cake> ();
            for (int i=0; i<size;i++)
            {
                InstantiateNewCake (newCake, newCakesList);
            }
            poolDictionary.Add (poolKey, newCakesList);
        } else
        {
            Debug.LogWarning ("You've tried to add a new Cake to our dictionary, but it already contains the object.");
        }
    }

    /// <summary>
    /// Get the selected pool object from an existing pool.
    /// </summary>
    /// <param name="Cake">The pool object.</param>
    /// <returns></returns>
    public Cake GetObjectFromPool (Cake Cake)
    {
        //Get the pool key for this object.
        int poolKey = Cake.GetInstanceID ();

        if (poolDictionary.ContainsKey (poolKey))     
        {
            //Get the list
            List<Cake> CakesList = poolDictionary[poolKey];

            //If the object exists return the first available one.
            //If we don't find any abailable object we will instantiate a new one.
            //This is why it is very important to disable the Cakes
            for (int i=0; i<CakesList.Count; i++)
            {
                if (CakesList[i].enabled == false)
                    return CakesList[i];
            }

            //Now, spawn a new one, if we don't find anyone
            InstantiateNewCake (Cake, CakesList);

            //return the last object added to the list
            return CakesList[CakesList.Count - 1];
        } else
        {
            Debug.LogError ("You are trying to get a Cake that doesn't exist in our poolDictionary. Please make sure that you add the pool object before trying to get them.");
            return null;
        }
    }

    /// <summary>
    /// Instantiates a new Cake and adds it to the correct list.
    /// </summary>
    /// <param name="CakeToSpawn">Object to instnatiate.</param>
    /// <param name="poolListToAdd">List to add.</param>
    private void InstantiateNewCake (Cake CakeToSpawn, List<Cake> poolListToAdd)
    {
        //New Cake
        Cake newCake = (Cake) Instantiate (CakeToSpawn, Vector3.zero, Quaternion.identity);
        //First, we set it to be disabled
        //newCake.DisableCake ();
        //Add to the pool list
        poolListToAdd.Add (newCake);
    }

    /// <summary>
    /// Disables all pool object selected.
    /// This is called, for example, when the player chooses to continue the game.
    /// </summary>
    /// <param name="Cake">The pool object to disable</param>
    public void DisableCakeList (Cake Cake)
    {
        //Get the pool key of this object
        int poolKey = Cake.GetInstanceID ();

        //If it exists, then for each Cake in the list, disable it
        if (poolDictionary.ContainsKey (poolKey) == true)
        {
            for (int i = 0; i < poolDictionary[poolKey].Count; i++)
            {
               // poolDictionary[poolKey][i].DisableCake ();  
            }
        }
        else
        {
            Debug.LogWarning ("You've tried to disable all pool objects but you didn't add the current pool object to the dictionary. Pool object: " + Cake.name);
        }
    }
}