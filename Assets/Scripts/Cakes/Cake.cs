﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cake : MonoBehaviour {

    public Score
        m_CatchScore,
        m_EatScore,
        m_LostScore;
       

    public enum ScoreGetType
    {
        EatScoreType,
        CatchScoreType,
        LostScoreType,
        CoinScoreType
    }

    internal virtual void LostCake()
    {
        
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.LostScoreType);
        Destroy(gameObject);
    }

    public Cake GetCake()
    {
        return this;
    }

    internal virtual void EatCake()
    {
       
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.EatScoreType);
        Destroy(gameObject);
    }

    internal virtual void CatchCake()
    {
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.CatchScoreType);
        Destroy(gameObject);
    }
}
