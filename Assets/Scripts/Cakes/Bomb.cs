﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : Cake {

    internal override void LostCake()
    {
        //Exlpose
        ExploseEffectController.instance.StartExplose(transform.position);
        base.LostCake();
       
    }

    internal override void EatCake()
    {
        //Lose game
        ExploseEffectController.instance.StartExplose(transform.position);
        base.EatCake();
    }

    internal override void CatchCake()
    {

        base.CatchCake();
    }
}
