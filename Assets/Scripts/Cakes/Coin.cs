﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Cake {

    internal override void LostCake()
    {
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.LostScoreType);
        Destroy(gameObject);
    }

    internal override void EatCake()
    {
       
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.EatScoreType);
        Destroy(gameObject);
    }

    internal override void CatchCake()
    {

        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.CoinScoreType);
        Destroy(gameObject);
    }
}
