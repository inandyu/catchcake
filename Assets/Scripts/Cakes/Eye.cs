﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eye : Cake {

    internal override void LostCake()
    {
        //Exlpose
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.LostScoreType);
        Destroy(gameObject);
    }

    internal override void EatCake()
    {

        EyeEffectController.EnableEye();
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.EatScoreType);
        Destroy(gameObject);
    }

    internal override void CatchCake()
    {
        EyeEffectController.EnableEye();
        CatchCakeSceneController.instance.TakeCake(this, ScoreGetType.CatchScoreType);
        Destroy(gameObject);
    }
}
