﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using System.Collections.Generic;

/// <summary>
/// Save System Manager. 
/// Helps with loading/saving data.
/// </summary>
public static class SaveSystemManager
{
    /// <summary>
    /// Intialize player data (games played, coins, etc)
    /// </summary>
    public static void InitializePlayerData ()
    {
        JSONNode playerDataJSON = JSON.Parse ("{}");

        playerDataJSON["PlayerData"]["NumberOfGamesPlayed"].AsInt = 0;
        playerDataJSON["PlayerData"]["Highscore"].AsInt = 0;  
    
          
        PlayerPrefs.SetString (GameSettings.PLAYER_STATS_PP, playerDataJSON.ToJSON (1));
        PlayerPrefs.Save ();
        Debug.Log ("Initialized player data: " + PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));
    }

    /// <summary>
    /// Add games played.
    /// </summary>
    /// <param name="value"></param>
    public static void AddGamesPlayedPlayerData (int value)
    {
        if (PlayerPrefs.HasKey (GameSettings.PLAYER_STATS_PP))
        {
            JSONNode playerDataJSON = JSON.Parse (PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));

            playerDataJSON["PlayerData"]["NumberOfGamesPlayed"].AsInt += value;
            PlayerPrefs.SetString (GameSettings.PLAYER_STATS_PP, playerDataJSON.ToJSON (1));
            PlayerPrefs.Save ();
            Debug.Log ("Saved player data: " + PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));
        }
        else
        {
            Debug.LogError ("You are trying to get info from player data but the player prefs is null is not initialized");
        }
    }

    /// <summary>
    /// Returns the number of games played
    /// </summary>
    /// <returns></returns>
    public static int GetNumberOfGamesPlayed ()
    {
        if (PlayerPrefs.HasKey (GameSettings.PLAYER_STATS_PP))
        {
            JSONNode playerDataJSON = JSON.Parse (PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));

            return playerDataJSON["PlayerData"]["NumberOfGamesPlayed"].AsInt;
        }
        else
        {
            Debug.LogError ("You are trying to get info from player data but the player prefs is null is not initialized");
            return 0;
        }
    }

    /// <summary>
    /// Sets a new highscore and save it to player prefs.
    /// </summary>
    /// <param name="newHighscore">The new highscore</param>
    public static void SetHighscore (int newHighscoreEat, int newHighscoreCatch)
    {
        if (PlayerPrefs.HasKey (GameSettings.PLAYER_STATS_PP))
        {
            JSONNode playerDataJSON = JSON.Parse (PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));

            playerDataJSON["PlayerData"]["Highscore"]["Eat"].AsInt = newHighscoreEat;
            playerDataJSON["PlayerData"]["Highscore"]["Catch"].AsInt = newHighscoreCatch;
            PlayerPrefs.SetString (GameSettings.PLAYER_STATS_PP, playerDataJSON.ToJSON (1));
            PlayerPrefs.Save ();
            Debug.Log ("Saved player data: " + PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));
        }
        else
        {
            Debug.LogError ("You are trying to get info from player data but the player prefs is null is not initialized");
        }
    }

    /// <summary>
    /// Returns the current highscore Eat.
    /// </summary>
    /// <returns>Returns highscore.</returns>
    public static int GetHighscoreEat ()
    {
        if (PlayerPrefs.HasKey (GameSettings.PLAYER_STATS_PP))
        {
            JSONNode playerDataJSON = JSON.Parse (PlayerPrefs.GetString (GameSettings.PLAYER_STATS_PP));

            return playerDataJSON["PlayerData"]["Highscore"]["Eat"].AsInt;
        }
        else
        {
            Debug.LogError ("You are trying to get info from player data but the player prefs is null is not initialized");
            return 0;
        }
    }

    /// <summary>
    /// Returns the current highscore Catch.
    /// </summary>
    /// <returns>Returns highscore.</returns>
    public static int GetHighscoreCatch()
    {
        if (PlayerPrefs.HasKey(GameSettings.PLAYER_STATS_PP))
        {
            JSONNode playerDataJSON = JSON.Parse(PlayerPrefs.GetString(GameSettings.PLAYER_STATS_PP));

            return playerDataJSON["PlayerData"]["Highscore"]["Catch"].AsInt;
        }
        else
        {
            Debug.LogError("You are trying to get info from player data but the player prefs is null is not initialized");
            return 0;
        }
    }
}
