﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Sounds managment.
/// Plays the sounds that you want.
/// </summary>
[RequireComponent (typeof (AudioSource))]
public class SoundManager : MonoBehaviour
{
    public static SoundManager singleton { get; private set; } //Singleton

    public bool musicState; //music state on/off

    [Header ("Audio Sources")]
    //Audio Sources
    public AudioSource backgroundAudioSource; //The background audio source
    public AudioSource fxAudioSource; //FX Audio source
    private bool sameAudioSources = false; //Check if we don't have the same audio source 

    [Header ("Audio clips")]
    public AudioClip menuBackgroundMusic;  //Menu background music
    public AudioClip gameBackgroundMusic; //Game background music
    
    public AudioClip buttonClickSound; //Button click sound
    public AudioClip gameOverSound; //Game over fx
    public AudioClip clickOnDestroyObjectSound; //Click on a new destroyable object sound    
    public AudioClip clickOnPowerUp; //Click on a power up
    public AudioClip explosionSound; //The sound that is played when the clickableObjects hit the target  

    [Header ("Random pitch")]
    //I tried to randomize a bit sound effects.
    public bool randomizeSoundPitch = true; //Randomize the pitch of the sound
    public float lowPitchRange = 0.9f; //Lowest pitch
    public float highPitchRange = 1.1f; //highest pitch

    /// <summary>
    /// Unity Awake function.
    /// </summary>
    private void Awake ()
    {
        if (singleton == null)
        {
            //If singleton doesnt exist then intialize it
            singleton = this;
            DontDestroyOnLoad (this.gameObject);
        }
        else
        {
            //Else destroy it. (This object will be present in all scenes)
            DestroyImmediate (this.gameObject);
        }

        //Lets set the audio sources
        if (backgroundAudioSource == fxAudioSource)
        {
            sameAudioSources = true;
            Debug.LogError ("You have set the fx audio source the same as the background audio source!. We won't play audio effects.");
        }
    }

    /// <summary>
    /// Start function.
    /// </summary>
    private void Start ()
    {
        //Set the sound state based on player's preferences.
        SetSoundState ();
    }

    /// <summary>
    /// Sets the sound state based on what player chose.
    /// </summary>
    private void SetSoundState ()
    {
        backgroundAudioSource.volume = fxAudioSource.volume = PlayerPrefs.GetInt (GameSettings.SOUND_STATE_PP);
        musicState = PlayerPrefs.GetInt (GameSettings.SOUND_STATE_PP) == 1 ? true : false;
    }

    /// <summary>
    /// Update the music state.
    /// </summary>
    /// <param name="state"></param>
    public void UpdateMusicState (bool state)
    {
        Debug.Log ("Music state: " + state);
        PlayerPrefs.SetInt (GameSettings.SOUND_STATE_PP, state == true ? 1 : 0);
        musicState = state;
        SetSoundState ();
    }

    /// <summary>
    /// Plaies an audioclip. Will be called from other classes
    /// </summary>
    /// <param name="audioClip">Audio clip.</param>
    /// <param name="randomizePitch">If set to <c>true</c> randomize pitch.</param>
    public void PlaySoundFX (AudioClip audioClip)
    {
        //Lets make sure that we don't use the same audio source for both background and sfx
        if (sameAudioSources == false)
        {
            //Make sure that the audio clip that we want to play isn't null.
            if (audioClip != null)
            {
                fxAudioSource.clip = audioClip;
                fxAudioSource.loop = false; //just to be sure

                if (randomizeSoundPitch)
                {
                    float randomPitch = Random.Range (lowPitchRange, highPitchRange);
                    fxAudioSource.pitch = randomPitch;
                }

                fxAudioSource.Play ();
            }
            else
            {
                Debug.LogError ("You have tried to play a sound effect but it is not assigned in the inspector.");
            }
        } else
        {
            Debug.LogError ("Make sure that you have assigned the audio sources correct. Please make sure that you have 2 different audio sources!");
        }
    }

    /// <summary>
    /// Plaies the background theme.
    /// </summary>
    /// <param name="backgroundSound">Background sound.</param>
    public void PlayBackgroundMusic (AudioClip backgroundSound)
    {
        if (backgroundSound != null)
        {
            if (backgroundAudioSource.clip != backgroundSound)
            {
                backgroundAudioSource.loop = true;
                backgroundAudioSource.clip = backgroundSound;
                backgroundAudioSource.playOnAwake = false;
                backgroundAudioSource.Play ();
            }
        } else
        {
            Debug.LogError ("You have tried to play a background music but it is null.");
        }
    }
}

