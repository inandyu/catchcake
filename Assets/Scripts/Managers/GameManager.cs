﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject CatchCake_go;
    public GameObject MainMenu_go;

    public static GameManager instance;

    public GameScore gameScore;

    MainWindowController mainWindow;

    // Use this for initialization
    void Start () {
        if(instance == null)
        {
            instance = this;
        }

        mainWindow = MainMenu_go.GetComponentInChildren<MainWindowController>(true);
        
        gameScore.Init();

        OpenMainMenu();

    }
	
	public void StartCatchCake()
    {
        CatchCake_go.SetActive(true);
        MainMenu_go.SetActive(false);
        mainWindow.Close();
    }

    public void OpenMainMenu()
    {
        CatchCake_go.SetActive(false);
        MainMenu_go.SetActive(true);
        mainWindow.Open();
    }

   
}
