﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ADS
using UnityEngine.Advertisements; // only compile Ads code on supported platforms
#endif

public class AdsManager : MonoBehaviour
{

    static System.Action m_HandleShowResult;

    public static AdsManager instance;

    //public PanelUINoInternetController panelNoInternet;

    private void Start()
    {
        if (instance == null)
            instance = this;

       // panelNoInternet.Initialization();
       // panelNoInternet.Close();
    }

    public void ShowRewardedAd(System.Action handleShowResult)
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //panelNoInternet.Open();
        }
        else
        {
            m_HandleShowResult = handleShowResult;
            const string RewardedPlacementId = "rewardedVideo";

#if UNITY_ADS
            if (!Advertisement.IsReady(RewardedPlacementId))
            {
                Debug.Log(string.Format("Ads not ready for placement '{0}'", RewardedPlacementId));
                return;
            }

            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show(RewardedPlacementId, options);
#endif
        }
    }

#if UNITY_ADS
    private void HandleShowResult(ShowResult result)
    {
       
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                m_HandleShowResult();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

#endif
}
