﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Initializes player's data.
/// </summary>
public class InitializeManager : MonoBehaviour
{
    /// <summary>
    /// Unity Awake function.
    /// </summary>
    private void Awake ()
    {
        //If we didn't set the sound until now do it.
        if (PlayerPrefs.HasKey (GameSettings.SOUND_STATE_PP) == false)
            InitializeSoundState ();

        //If we didn't set the Player stats until now do it.
        if (PlayerPrefs.HasKey (GameSettings.PLAYER_STATS_PP) == false)
            SaveSystemManager.InitializePlayerData ();
    } 

    /// <summary>
    /// Initializes sound state.
    /// Set the sound state to 1 (the music will be playing);
    /// </summary>
    private void InitializeSoundState ()
    {
        PlayerPrefs.SetInt (GameSettings.SOUND_STATE_PP, 1);
        PlayerPrefs.Save ();
        Debug.Log ("Initialized sound state. Sound state: " + PlayerPrefs.GetInt (GameSettings.SOUND_STATE_PP));
    }
}
