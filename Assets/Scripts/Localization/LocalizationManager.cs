﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.IO;
using System;

public class LocalizationManager : MonoBehaviour
{

    public static LocalizationManager instance;

    private Dictionary<string, string> localizedText;
    private bool isReady = false;
    private string missingTextString = "Localized key not found";

    private List<String> LanguageDictinary = new List<string>() { AppSettings.LocalizationEn, AppSettings.LocalizationRu };

    public UnityAction OnNewLanguage;


    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    public IEnumerator LoadLocalizedText(string fileName = "")
    {
       

        /* if (!PlayerPrefs.HasKey(AppSettings.LanguageSetting))
         {

             if (Application.systemLanguage == SystemLanguage.Russian)
             {

                 PlayerPrefs.SetInt(AppSettings.LanguageSetting, (int)LanguageType.Russian);


             }
             //Otherwise, if the system is English, output the message in the console
             else//(Application.systemLanguage == SystemLanguage.English)
             {
                 PlayerPrefs.SetInt(AppSettings.LanguageSetting, (int)LanguageType.English);
             }

         }*/
        fileName = GetLanguage();



        localizedText = new Dictionary<string, string>();
        //string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
        /* #if UNITY_ANDROID && !UNITY_EDITOR

                 string filePath ="jar:file://" + Application.dataPath + "!/assets/"+ fileName;
                 // string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

 #elif UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
             string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

         #elif UNITY_IOS
                 string filePath = Path.Combine(Application.dataPath + "/Raw", fileName);
         #endif
                 Debug.Log("filePath " + filePath);
         #if UNITY_ANDROID && !UNITY_EDITOR       
                WWW reader = new WWW (filePath);

                 yield return reader;


                 if (string.IsNullOrEmpty(reader.error))
                 {

                     string dataAsJson = reader.text;

         #else

          if (File.Exists(filePath))*/
        TextAsset Text = UnityEngine.Resources.Load(fileName) as TextAsset;
        if (Text != null)
        {
            string dataAsJson = Text.text;
            // string dataAsJson = File.ReadAllText(filePath, System.Text.Encoding.UTF8);

            //#endif

            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        }
        else
        {
            Debug.LogError("Cannot find file!");
        }

        if (instance == null)
        {
            Debug.LogError("instance == null");
        }

        isReady = true;

        if (OnNewLanguage != null)
        {
            Debug.LogError("OnNewLanguage");
            OnNewLanguage();
        }

        yield return null;
    }

    public string GetLanguage()
    {
        int indexLanguange = (int)PlayerPrefs.GetInt(AppSettings.LanguageSetting, 0);
        return LanguageDictinary[indexLanguange];
    }

    public LanguageType GetLanguageEnum()
    {
        return (LanguageType)PlayerPrefs.GetInt(AppSettings.LanguageSetting);
    }

    public void SetLanguage(LanguageType value)
    {
        PlayerPrefs.SetInt(AppSettings.LanguageSetting, (int)value);

        StartCoroutine(LoadLocalizedText());
    }

    public enum LanguageType
    {
        English,
        Russian
    }

    public string GetLocalizedValue(string key)
    {
        if (key == null || key == "")
        {
            key = missingTextString;
        }
        string result = key;

        if (localizedText == null)
        {
            Debug.LogError("localizedText not init");
        }
        else if (localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;

    }

    public bool GetIsReady()
    {
        return isReady;
    }

    public static void SaveNewLocalizedText(string fileName, string Key, string value)
    {
        List<LocalizationItem> localizedText = new List<LocalizationItem>();
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath, System.Text.Encoding.UTF8);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(new LocalizationItem(loadedData.items[i].key, loadedData.items[i].value));
            }

            localizedText.Add(new LocalizationItem(Key, value));
            loadedData.items = localizedText.ToArray();
            Debug.Log(localizedText.ToArray());
            string jsonString = JsonUtility.ToJson(loadedData);
            File.WriteAllText(filePath, jsonString, System.Text.Encoding.UTF8);

        }
        else
        {
            Debug.LogError("Cannot find file!");
        }


    }


}
