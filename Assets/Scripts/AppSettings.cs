﻿

public static class AppSettings {

    public static string LanguageSetting = "LanguageSetting";
    public static string LocalizationRu = "localizedText_ru";
    public static string LocalizationEn = "localizedText_en";
}
