﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if DOTweenChecker
using DG.Tweening;
#endif

/// <summary>
/// Menu scene UI Controller controls the user interfaces in our Menu Scene and initializes them.
/// </summary>
public class GameSceneController : MonoBehaviour
{
   /* public static GameSceneController singleton { get; private set; } //singleton

    public CustomUIPanel currentPanel; //current selected panel

    [Space (25)]
    [Header ("MAIN MENU PANEL")]
    public CustomUIPanel mainMenuPanel; //Main menu panel with its settings
    public Button rateButton; //Rate button
    public Button moreGamesButton; //More games button

    [Space (25)]
    [Header ("SETTINGS PANEL")]
    public CustomUIPanel settingsPanel;
    public Toggle musicToggle;
    public RectTransform contactUsHolder;
    public Text contactUsText;
    public RectTransform numberOfGamesPlayedHolder;
    public Text numberOfGamesPlayedText;

    [Header ("TUTORIAL")]
    public RectTransform tutorialPanel;

    [Space (25)]
    [Header ("GAME HUD PANEL")]
    public CustomUIPanel gameHUDPanel;
    public Text livesText;
    public Text scoreText;

    [Space (25)]
    [Header ("GAME OVER PANEL")]
    public CustomUIPanel gameOverPanel;
    public Text scoreObtainedText;
    public Text highScoreText;
    public RectTransform newHighscoreText;
    public Button continueButton;


    [Space (25)]
    [Header ("OTHER SETTINGS")]
    public bool letUserChooseTheSamePanel = false; //Lets the user go to the same panel
    public bool enablePanelsTransitions = true; //Enable panels transitions

    /// <summary>
    /// Unity Awake callback.
    /// </summary>
    private void Awake ()
    {
        if (singleton == null)
            singleton = this;
        else
            DestroyImmediate (this.gameObject);
    }

    /// <summary>
    /// Unity Start function.
    /// </summary>
    private void Start ()
    {
        //We will have a problem with the time scale, if we "come" from a level scene, because timescale is a static variable, and we are setting it to 0 when the player lose a game
        //So, we will make sure that everytime we load the menu scene we set the time scale to 1.
        Time.timeScale = 1f;
        
        //Initialize the user interfaces (Main menu panel, settings panel).
        //Set the buttons based on developer's choice
      

        //Play menu music background
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlayBackgroundMusic (SoundManager.singleton.menuBackgroundMusic);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");   
    }

    

    /// <summary>
    /// Updates the lives text.
    /// </summary>
    /// <param name="numberOfLives">Number of lives to show.</param>
    public void UpdateLivesText (int numberOfLives)
    {
        livesText.text = "Lives: " + numberOfLives.ToString ();
    }

    /// <summary>
    /// Updates the score text.
    /// </summary>
    /// <param name="score">Score to update.</param>
    public void UpdateScoreText (int score)
    {
        scoreText.text = "Score: " + score.ToString ();
    }

    /// <summary>
    /// Set tutorial panel.
    /// </summary>
    /// <param name="isTutorialOn"></param>
    public void SetTutorialPanel (bool isTutorialOn)
    {
        //Set the tutorial panel on/off
        tutorialPanel.gameObject.SetActive (isTutorialOn);
    }

    //Update panels
    #region UPDATE_PANELS
    /// <summary>
    /// Updates game over panel.
    /// </summary>
    /// <param name="scoreObtained">Score obtained.</param>
    /// <param name="highscore">High score obtained.</param>
    /// <param name="newHighscore">New highscore</param>
    public void UpdateGameOverPanel (int scoreObtained, int highscore, bool newHighscore)
    {
        //Update the infos
        scoreObtainedText.text = "Score: " + scoreObtained.ToString ();
        highScoreText.text = "Highscore: " + highscore.ToString ();
        newHighscoreText.gameObject.SetActive (newHighscore);

        //Lets check if we have internet connection and set the rate/continue buttons. 
        //If we don't have internet connection we don't want to let the user press these buttons.
        rateButton.interactable = continueButton.interactable = Utilities.CheckIfInternetConnection ();

        //If we didn't continue the game then let the button interactable
        //continueButton.interactable = (!GameManager.singleton.continuedGame);

        //Now, based on developer's game preferences set the continue button and the rate game button
        //Check if the gamepreferences class exits
        if (GamePreferences.singleton != null)
        {
            //Continue by watching a video button.
            //If the enable ads are on then show the button else hide it
#if UNITY_ANDROID || UNITY_IOS || UNITY_WPA
           // continueButton.gameObject.SetActive (GamePreferences.singleton.ENABLE_END_OF_LEVEL_UNITY_ADS && !GameManager.singleton.continuedGame);
#endif
            //Same for the rate button
            rateButton.gameObject.SetActive (GamePreferences.singleton.ENABLE_RATE_BUTTON);
        }
        else
        {
            Debug.LogError ("We couldn't find the GamePreferences. Either you didn't start from the SplashScene, where it initializes, either you didn't create one.\nWe will use our default values.");
        }

        //Unity ads are available only for wp, android and iOS so we will set the continue button of for the rest of the platforms
#if (UNITY_ANDROID || UNITY_IOS || UNITY_WP) && UNITY_ADS
        //Keep the current state if unity ads are enabled and we are on a mobile platform either way set it to false
        continueButton.gameObject.SetActive (continueButton.gameObject.activeSelf);
#elif UNITY_EDITOR
        continueButton.gameObject.SetActive (true);
#else 
        continueButton.gameObject.SetActive (false);
#endif

        //Activate game over panel
        ActivatePanel (gameOverPanel);
    }

    /// <summary>
    /// Activates the game hud panel.
    /// </summary>
    public void ActivateGameHudPanel ()
    {
        ActivatePanel (gameHUDPanel);
    }
    #endregion

    #region UI_Buttons  
    /// <summary>
    /// Music state toggle. Set this to your music state toggle in settings panel.
    /// </summary>
    public void UIToggle_SetMusicState ()
    {
        if (SoundManager.singleton != null)
            SoundManager.singleton.UpdateMusicState (musicToggle.isOn);
        else
            Debug.LogWarning ("We have tried to set the sound state but the sound manager is null.");
    }

    /// <summary>
    /// Rate button. Set this to your rate button on click event.
    /// </summary>
    public void UIButton_Rate ()
    {
        //Play button click sfx
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");

        if (GamePreferences.singleton != null)
        {
            //Now, based on the current platform open the rate link.
#if UNITY_ANDROID
            Application.OpenURL (GamePreferences.singleton.URL_ANDROID_GAME);
#endif
#if UNITY_IOS
            Application.OpenURL (GamePreferences.singleton.URL_IOS_GAME);
#endif
#if UNITY_STANDALONE_OSX
            Application.OpenURL (GamePreferences.singleton.URL_MAC_GAME);
#endif
#if UNITY_WEBGL
            Application.OpenURL (GamePreferences.singleton.URL_WEBGL_GAME);
#endif
#if UNITY_STANDALONE_WIN
            Application.OpenURL (GamePreferences.singleton.URL_WINDOWS_GAME);
#endif
#if UNITY_WSA
            Application.OpenURL (GamePreferences.singleton.URL_WP_GAME);
#endif
        } else
        {
            Debug.LogError ("You don't have the game preferences singleton in this scene. Please make sure that you start from the SplashScene. We will use the default settings from GameSettings class.");
#if UNITY_ANDROID
            Application.OpenURL (GameSettings.URL_ANDROID_GAME);
#endif
#if UNITY_IOS
            Application.OpenURL (GameSettings.URL_IOS_GAME);
#endif
#if UNITY_STANDALONE_OSX
            Application.OpenURL (GameSettings.URL_MAC_GAME);
#endif
#if UNITY_WEBGL
            Application.OpenURL (GameSettings.URL_WEBGL_GAME);
#endif
#if UNITY_STANDALONE_WIN
            Application.OpenURL (GameSettings.URL_WINDOWS_GAME);
#endif
#if UNITY_WSA
            Application.OpenURL (GameSettings.URL_WP_GAME);
#endif
        }
    }

    /// <summary>
    /// More games button. Set this to your more games button on click event.
    /// </summary>
    public void UIButton_MoreGames ()
    {
        //Play button click sfx
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");

        if (GamePreferences.singleton != null)
        {
            //Now, based on the current platform open the more games link.
#if UNITY_ANDROID
            Application.OpenURL (GamePreferences.singleton.URL_ANDROID_MORE_GAMES);
#endif
#if UNITY_IOS
            Application.OpenURL (GamePreferences.singleton.URL_IOS_MORE_GAMES);
#endif
#if UNITY_STANDALONE_OSX
            Application.OpenURL (GamePreferences.singleton.URL_MAC_MORE_GAMES);
#endif
#if UNITY_WEBGL
            Application.OpenURL (GamePreferences.singleton.URL_WEBGL_MORE_GAMES);
#endif
#if UNITY_STANDALONE_WIN
            Application.OpenURL (GamePreferences.singleton.URL_WINDOWS_MORE_GAMES);
#endif
#if UNITY_WSA
            Application.OpenURL (GamePreferences.singleton.URL_WP_MORE_GAMES);
#endif
        }
        else
        {
            Debug.LogError ("You don't have the game preferences singleton in this scene. Please make sure that you start from the SplashScene. We will use the default settings from GameSettings class.");
#if UNITY_ANDROID
            Application.OpenURL (GameSettings.URL_ANDROID_MORE_GAMES);
#endif
#if UNITY_IOS
            Application.OpenURL (GameSettings.URL_IOS_MORE_GAMES);
#endif
#if UNITY_STANDALONE_OSX
            Application.OpenURL (GameSettings.URL_MAC_MORE_GAMES);
#endif
#if UNITY_WEBGL
            Application.OpenURL (GameSettings.URL_WEBGL_MORE_GAMES);
#endif
#if UNITY_STANDALONE_WIN
            Application.OpenURL (GameSettings.URL_WINDOWS_MORE_GAMES);
#endif
#if UNITY_WSA
            Application.OpenURL (GameSettings.URL_WP_MORE_GAMES);
#endif
        }
    }

    /// <summary>
    /// Play butoon from the main menu.
    /// </summary>
    public void UIButton_PlayButton ()
    {
       // GameManager.singleton.StartGame ();

        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
    }

    /// <summary>
    /// Home button from game over screen.
    /// </summary>
    public void UIButton_HomeButton ()
    {
        //Fade into game scene
        //GameManager.singleton.GoToHomeScreen ();
    }

    /// <summary>
    /// Retry button from game over screen.
    /// </summary>
    public void UIButton_RetryButton ()
    {
       // GameManager.singleton.RetryGame ();
    }

    /// <summary>
    /// Continue button. Available on Android and iOS.
    /// </summary>
    public void UIButton_ContinueButton ()
    {
#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
        if (AdsManager.singleton != null)
        {
            AdsManager.singleton.UNITY_ADS_ShowRewardedVideo_To_Continue_Game ();
        }
        else
        {
#endif
            Debug.LogError ("You have tried to show an ad without the AdsManager in scene.");

#if UNITY_EDITOR
           // GameManager.singleton.ContinueGame ();
#endif        

        //Play button click sfx
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");
    }

    /// <summary>
    /// Go to settings button.
    /// </summary>
    public void UIButton_GoToSettings ()
    {
        ActivatePanel (settingsPanel);

        //Play button click sfx
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");
    }

    /// <summary>
    /// Back button for settings panel or any other panel that you want to add.
    /// </summary>
    public void UIButton_GoToMainMenuPanel ()
    {
        ActivatePanel (mainMenuPanel);

        //Play button click sfx
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);
        else
            Debug.LogWarning ("We have tried to play a sound but the sound manager is null.");
    }

    /// <summary>
    /// Sets the tutorial off. 
    /// Button attached to the start button from the tutorial panel.
    /// </summary>
    public void UIButton_SetTutorialOff ()
    {
      //  GameManager.singleton.SetTutorialOff ();
    }

    /// <summary>
    /// Quit button. Set it to your quit button.
    /// </summary>
    public void UIButton_Quit ()
    {
        if (SoundManager.singleton != null)
            SoundManager.singleton.PlaySoundFX (SoundManager.singleton.buttonClickSound);

        Application.Quit ();
    }
#endregion

    #region PANELS_ACTIVATION
    /// <summary>
    /// Activates a panel based on the panel's settings
    /// </summary>
    /// <param name="nextPanel">The panel to mvoe on.</param>
    public void ActivatePanel (CustomUIPanel nextPanel)
    {
        if (letUserChooseTheSamePanel == false && currentPanel == nextPanel)
            return;

        if (nextPanel.panelContainer == null)
        {
            Debug.LogError ("You didn't set the panel that you want to go to. Set it in inspector (Settings Panel, Main Menu Panel or Level Select Panel");
            return;
        }

        //If we've set the panels transition on then let it do the transition based on each panel settings
        if (enablePanelsTransitions)
        {
            //We are just initializing this. We will modify based on transition type.
            Vector2 nextPanel_anchorMinOutOfScreen = Vector2.one;
            Vector2 nextPanel_anchorMaxOutOfScreen = Vector2.one;

            //Lets set the min and max anchors of our next panel out of the screen based on panel settings
            //This is the point where our next panel appears!
            //We set where the panel appears by setting the transition type. (for example: Up to down - the animation starts from the top of the screen and go to the middle)
            switch (nextPanel.transitionType)
            {
                case TransitionType.FROM_DOWN_TO_UP:
                    nextPanel_anchorMinOutOfScreen = new Vector2 (0f, -1f);
                    nextPanel_anchorMaxOutOfScreen = new Vector2 (1f, 0f);
                    break;

                case TransitionType.FROM_UP_TO_DOWN:
                    nextPanel_anchorMinOutOfScreen = new Vector2 (0f, 1f);
                    nextPanel_anchorMaxOutOfScreen = new Vector2 (1f, 2f);
                    break;

                case TransitionType.FROM_LEFT_TO_RIGHT:
                    nextPanel_anchorMinOutOfScreen = new Vector2 (-1f, 0f);
                    nextPanel_anchorMaxOutOfScreen = new Vector2 (0f, 1f);
                    break;

                case TransitionType.FROM_RIGHT_TO_LEFT:
                    nextPanel_anchorMinOutOfScreen = new Vector2 (1f, 0f);
                    nextPanel_anchorMaxOutOfScreen = new Vector2 (2f, 1f);
                    break;

                default:

                    break;
            }

            //Now lets set where the current panel goes based on settings that we have
            Vector2 currentPanel_anchorMin = Vector2.one;
            Vector2 currentPanel_anchorMax = Vector2.one;

            //Based on what we have chose for the current panel lets move it
            switch (currentPanel.transitionType)
            {
                case TransitionType.FROM_DOWN_TO_UP:
                    currentPanel_anchorMin = new Vector2 (0f, -1f);
                    currentPanel_anchorMax = new Vector2 (1f, 0f);
                    break;

                case TransitionType.FROM_UP_TO_DOWN:
                    currentPanel_anchorMin = new Vector2 (0f, 1f);
                    currentPanel_anchorMax = new Vector2 (1f, 2f);
                    break;

                case TransitionType.FROM_LEFT_TO_RIGHT:
                    currentPanel_anchorMin = new Vector2 (-1f, 0f);
                    currentPanel_anchorMax = new Vector2 (0f, 1f);
                    break;

                case TransitionType.FROM_RIGHT_TO_LEFT:
                    currentPanel_anchorMin = new Vector2 (1f, 0f);
                    currentPanel_anchorMax = new Vector2 (2f, 1f);
                    break;

                default:

                    break;
            }


#if DOTweenChecker
            //Lets create the animation sequence using dotween.
            Sequence animationSequence = DOTween.Sequence ();

            //Make the animation sequence independent from unity's time scale.
            animationSequence.SetUpdate (true);

            //Now that we have defined our anchors min and max lets moeve the current panel
            //We will have to move in the same time both anchors (min and max)
            //Also set the ease and duration for the animation based on current settings.
            animationSequence.Append (currentPanel.panelContainer.DOAnchorMax (currentPanel_anchorMax, currentPanel.transitionDuration).SetEase (currentPanel.transitionEase));
            animationSequence.Join (currentPanel.panelContainer.DOAnchorMin (currentPanel_anchorMin, currentPanel.transitionDuration).SetEase (currentPanel.transitionEase).OnComplete (() =>
            {
                //Lets set the next panel start position and activate the gameobject holder.
                nextPanel.panelContainer.anchorMax = nextPanel_anchorMaxOutOfScreen;
                nextPanel.panelContainer.anchorMin = nextPanel_anchorMinOutOfScreen;

                //We will also have to deactive the current panel and activate the new one.
                currentPanel.panelContainer.gameObject.SetActive (false);
                nextPanel.panelContainer.gameObject.SetActive (true);

                //Now set the next panel to be the current panel. 
                currentPanel = nextPanel;
            }));

            //We are always moving the next panel's anchors to Vector2.one (1,1) for max anchors and Vector2.zero (0, 0) for min anchors
            //So we know that they will always be in the middle of the screen
            animationSequence.Append (nextPanel.panelContainer.DOAnchorMax (Vector2.one, nextPanel.transitionDuration).SetEase (nextPanel.transitionEase));
            animationSequence.Join (nextPanel.panelContainer.DOAnchorMin (Vector2.zero, nextPanel.transitionDuration).SetEase (nextPanel.transitionEase));

#else
            //If we don't imported dotween for panels transitions just pop up the next panel.
            //We will also have to deactive the current panel and activate the new one.
            currentPanel.panelContainer.gameObject.SetActive (false);
            nextPanel.panelContainer.gameObject.SetActive (true);

            //Now set the next panel to be the current panel. 
            currentPanel = nextPanel;
#endif
        } else
        {
            //If we don't allow panels transitions just pop up the next panel.
            //We will also have to deactive the current panel and activate the new one.
            currentPanel.panelContainer.gameObject.SetActive (false);
            nextPanel.panelContainer.gameObject.SetActive (true);

            //Now set the next panel to be the current panel. 
            currentPanel = nextPanel;
        }
    }
#endregio*/
}

[System.Serializable]
public class CustomUIPanel
{
    public RectTransform panelContainer; //The panel container
    public float transitionDuration = 1f; //The transition duration
#if DOTweenChecker
    public Ease transitionEase = Ease.OutBounce; //The transition ease
#endif
    public TransitionType transitionType = TransitionType.FROM_UP_TO_DOWN; //Transition type (the point where the panel stars)
}

public enum TransitionType
{
    FROM_RIGHT_TO_LEFT, //Stars from the right part of the scene and goes to left (stops in middle of the screen)
    FROM_LEFT_TO_RIGHT, //Stars from the left part of the scene and goes to right (stops in middle of the screen)
    FROM_UP_TO_DOWN, //Stars from the top part of the scene and goes to bottom (stops in middle of the screen)
    FROM_DOWN_TO_UP //Stars from the bottom part of the scene and goes to top (stops in middle of the screen)
}