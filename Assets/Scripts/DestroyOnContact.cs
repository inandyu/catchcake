﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour {

    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Cake")
        {
            Cake cake = other.GetComponent<Cake>();
            if(cake)
            {
                cake.LostCake();
            }
           
        }
    }
}
