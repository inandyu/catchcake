﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PoolObject class.
/// </summary>
public class PoolObject : MonoBehaviour
{
    public bool active; //is this pool object active or not?

    /// <summary>
    /// Disables a pool object.
    /// </summary>
    public void DisablePoolObject ()
    {
        this.active = false;
        this.gameObject.SetActive (false);
    }

    /// <summary>
    /// Enables a pool object.
    /// </summary>
    public void ActivatePoolObject ()
    {
        this.active = true;
        this.gameObject.SetActive (true);
    }
}
