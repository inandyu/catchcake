﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Score {

    public int ScoreValue, TimeValue;
    public int GetTime()
    {
        return ScoreValue * TimeValue;
    }
    public override string ToString()
    {
        return "ScoreValue: "+ ScoreValue + ";TimeValue: "+ TimeValue+".";
    }
}
