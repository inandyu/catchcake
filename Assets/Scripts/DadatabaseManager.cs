﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

using System.IO;

public class DatabaseManager : MonoBehaviour {

    #region database utilits
    public static string GetDatabase(string ItemDatabase)
    {
        //Debug.Log(ItemDatabase);

        TextAsset database = UnityEngine.Resources.Load(ItemDatabase) as TextAsset;
        //Debug.Log(database);
        return database == null ? "" : database.text;
    }
 

    public static Dictionary<string, JsonData> LoadDatabaseBySlugName(string name)
    {
        Dictionary<string, JsonData> result = new Dictionary<string, JsonData>();
        TextAsset Text = UnityEngine.Resources.Load(name) as TextAsset;
        if (Text == null)
        {
            Debug.LogError(name + " Not Found");
        }
        var enemiesJsonText = JsonMapper.ToObject(Text.text);



        if (enemiesJsonText.GetJsonType() != JsonType.Array)
        {
            return null;
        }

        if (enemiesJsonText.Count == 0)
        {
            return null;
        }

        for (int i = 0; i < enemiesJsonText.Count; i++)
        {
            result.Add(enemiesJsonText[i]["SlugName"].ToString(), enemiesJsonText[i]);
        }

        return result;
    }

    public static Dictionary<int, JsonData> LoadDatabaseByID(string name)
    {
        Dictionary<int, JsonData> result = new Dictionary<int, JsonData>();
        TextAsset Text = UnityEngine.Resources.Load(name) as TextAsset;
        if (Text == null)
        {
            Debug.LogError(name + " Not Found");
        }
        var JsonText = JsonMapper.ToObject(Text.text);



        if (JsonText.GetJsonType() != JsonType.Array)
        {
            return null;
        }

        if (JsonText.Count == 0)
        {
            return null;
        }

        for (int i = 0; i < JsonText.Count; i++)
        {
            result.Add((int)JsonText[i]["ID"], JsonText[i]);
        }

        return result;
    }

    public static Dictionary<int, JsonData> LoadDatabaseByNumber(string name)
    {
        Dictionary<int, JsonData> result = new Dictionary<int, JsonData>();
        TextAsset Text = UnityEngine.Resources.Load(name) as TextAsset;
        if (Text == null)
        {

            Debug.LogError(name + " Not Found");
        }

        Debug.LogError(name + " Text.text " + Text.text);
        var JsonText = JsonMapper.ToObject(Text.text);

        //Debug.Log("JsonText " + JsonMapper.ToJson(JsonText));

        if (JsonText.GetJsonType() != JsonType.Array)
        {
            Debug.LogError(name + " Not Array");
            return null;
        }

        if (JsonText.Count == 0)
        {
            Debug.LogError(name + " count 0");

            return null;
        }

        for (int i = 0; i < JsonText.Count; i++)
        {
            result.Add(i + 1, JsonText[i]);
        }

        return result;
    }

    #endregion
}
