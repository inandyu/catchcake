﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameScore {

    public int eatScore { get; protected set; }
    public int catchScore { get; protected set; }
    public int cointAmount { get; protected set; }

    public void Init()
    {
        eatScore = PlayerPrefs.GetInt("EatScore",0);
        catchScore = PlayerPrefs.GetInt("CatchScore", 0);
        cointAmount = PlayerPrefs.GetInt("CointAmount", 0);
    }

    public bool CheckEatScore(int newValue)
    {
        if (eatScore < newValue)
        {
            eatScore = newValue;
            PlayerPrefs.SetInt("EatScore", newValue);
            return true;
        }


        return false;
    }

    public bool CheckCatchScore(int newValue)
    {
        if (catchScore < newValue)
        {
            catchScore = newValue;
            PlayerPrefs.SetInt("CatchScore", newValue);
            return true;
        }

        return false;
    }

    public void AddCoin(int value)
    {
        if (value > 0)
            cointAmount += value;
    }
}
