﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NewBehaviourScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
       // Delegate<int> d1 = new Delegate<int>();
        Delegate<int>.Del del1;
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class Father<T1, T2>
{ }
public class Base
{
    public void M1() { }
    public void M2() { }
    
}
/// <summary>
/// 
/// </summary>
/// <typeparam name="T1"></typeparam>
/// <typeparam name="T2"></typeparam>
public class Child<T1, T2> : Father<T1, T2>
    where T1 : Base, IEnumerable<T1>, new()
    where T2 : struct, IComparable<T2>
{ }
class Delegate<T>
{
    public delegate T Del(T a, T b);
}