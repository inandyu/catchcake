﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FistCatch : MonoBehaviour {

   
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Cake")
        {
            other.transform.SetParent(this.transform);
            other.transform.localPosition = Vector3.zero;
            other.GetComponent<Rigidbody2D>().Sleep();
            
        }
    }
}
