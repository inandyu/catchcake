﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainWindowController : UIBaseController {

    public Text eatScore;
    public Text catchScore;
    public Button shopButton;
    public Text cointAmount;

    public override void Initialization()
    {
        base.Initialization();
       
    }

    public override void Open()
    {
        base.Open();

        eatScore.text = GameManager.instance.gameScore.eatScore.ToString();
        catchScore.text = GameManager.instance.gameScore.catchScore.ToString();
        cointAmount.text = GameManager.instance.gameScore.cointAmount.ToString();

        shopButton.onClick.AddListener(OpenShowWindow);
    }

    public void OpenShowWindow()
    {

    }
}
