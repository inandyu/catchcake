﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchCakeUIPauseMenuController : UIBaseController {

    public Button ContinueButton;
    public Button RestartButton;


    public override void Initialization()
    {
        base.Initialization();
        ContinueButton.onClick.AddListener(ContinueButtonClicked);
        RestartButton.onClick.AddListener(RestartButtonClicked);

        Close();
    }

    void ContinueButtonClicked()
    {
        CatchCakeSceneController.instance.ContinueGame();
    }

    void RestartButtonClicked()
    {
        CatchCakeSceneController.instance.RestartGame();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
