﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchCakeUIGameoverMenuController : UIBaseController {

    public Text m_EatScoreText;
    public Text m_CatchScoreText;
    
    public Image m_NewEatRecord, m_NewCatchRecord;

    public Button m_ContinueForAds;
    public Button m_OkButton;

    public override void Initialization()
    {
        base.Initialization();
    }
    public override void Open()
    {
        base.Open();

        m_EatScoreText.text = CatchCakeSceneController.instance.eatScore.ToString();
        bool enable = GameManager.instance.gameScore.CheckEatScore(CatchCakeSceneController.instance.eatScore);
        m_NewEatRecord.gameObject.SetActive(enable);

        enable = GameManager.instance.gameScore.CheckCatchScore(CatchCakeSceneController.instance.catchScore);
        m_CatchScoreText.text = CatchCakeSceneController.instance.catchScore.ToString();
        m_NewCatchRecord.gameObject.SetActive(enable);

        m_ContinueForAds.onClick.AddListener(ShowAdsForContinue);

        m_OkButton.onClick.AddListener(OkButtonClicked);
    }

    void OkButtonClicked()
    {
        CatchCakeSceneController.instance.RestartGame();
    }

    public void ShowAdsForContinue()
    {
        AdsManager.instance.ShowRewardedAd(ContinueAfterAds);
    }

    void ContinueAfterAds()
    {
        CatchCakeSceneController.instance.ContinueGame(5);
    }
}
