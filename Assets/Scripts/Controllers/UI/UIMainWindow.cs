﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainWindow : UIBaseController {

	public Button startButton;
    public override void Initialization()
    {
        base.Initialization();

        startButton.onClick.AddListener(startButtonClicked);
    }

    void startButtonClicked()
    {

    }
}
