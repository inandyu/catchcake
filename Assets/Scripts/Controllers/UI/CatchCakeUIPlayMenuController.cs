﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatchCakeUIPlayMenuController : UIBaseController
{

    public Button pauseButton;
    public Text scoreText, timerText;

    // Use this for initialization
    void Start () {

        UpdateUI();

        pauseButton.onClick.AddListener(PauseButtonClicked);
    }
	
    void UpdateUI()
    {
        if (CatchCakeSceneController.instance != null)
       { timerText.text = "TIME LEFT:\n" + Mathf.RoundToInt(CatchCakeSceneController.instance.timeLeft);
            scoreText.text = "catchScore: " + CatchCakeSceneController.instance.catchScore;
            scoreText.text += "\nlostScore: " + CatchCakeSceneController.instance.lostScore;
            scoreText.text += "\neatScore: " + CatchCakeSceneController.instance.eatScore;
        }
    }

    void PauseButtonClicked()
    {
        CatchCakeSceneController.instance.PauseGame();
    }

	// Update is called once per frame
	void FixedUpdate () {
        UpdateUI();

    }
}
