﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouthController : MonoBehaviour {

    public Camera cam;

    private float maxWidth;
    private bool canControl;
    Rigidbody2D rigid;
 
    public RectTransform m_MouthZoneControl;
    Vector3 targetPosition;
    // Use this for initialization
    void Start()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0.0f);
        Vector3 targetWidth = cam.ScreenToWorldPoint(upperCorner);
        float hatWidth = GetComponent<Renderer>().bounds.extents.x;
        maxWidth = targetWidth.x - hatWidth;
        canControl = true;
        rigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per physics timestep
    void FixedUpdate()
    {
        if (canControl)
        {
            
            if (m_MouthZoneControl.rect.Contains(cam.ScreenToWorldPoint(Input.mousePosition)))
            {
                Vector3 rawPosition = cam.ScreenToWorldPoint(Input.mousePosition);
                targetPosition = new Vector3(rawPosition.x, 0.0f, 0.0f);
                float targetWidth = Mathf.Clamp(targetPosition.x, -maxWidth, maxWidth);
                targetPosition = new Vector3(targetWidth, targetPosition.y, targetPosition.z);
                
            }
            rigid.MovePosition(targetPosition);
        }
    }

    public void ToggleControl(bool toggle)
    {
        canControl = toggle;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Cake")
        {
            Cake cake = other.GetComponent<Cake>();
            if (cake)
            {
                cake.EatCake();
            }
            
        }
    }

}
