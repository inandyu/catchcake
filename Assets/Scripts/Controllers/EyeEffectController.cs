﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeEffectController : MonoBehaviour {

    float topPosY= 7.87f;
    float time = 0;
    float timeToOff = 3;
    public static EyeEffectController instance;

    SpriteRenderer sprite;

    public void Start()
    {
        if(instance == null)
        {
            instance = this;
        }

        sprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    public static void EnableEye()
    {
        instance.StartEye();

    }


    public void StartEye()
    {
        time = timeToOff;
        gameObject.SetActive(true);

        Color c = sprite.color;
        c.a = 255;
        sprite.color = c;

        //SetPosition(CatchCakeSceneController.instance.nextSpawnPosition);
    }
    public void SetPosition(Vector3 pos)
    {
        pos.y = topPosY;
        transform.position = pos;
        

    }

    private void FixedUpdate()
    {
        if (time > 0)
        {
            time -= Time.fixedDeltaTime;
            SetPosition(CatchCakeSceneController.instance.nextSpawnPosition);
            Color c = sprite.color;
            c.a = (time / timeToOff);
            sprite.color = c;
        }
        else
        {
            gameObject.SetActive(false);
        }


    }


}
