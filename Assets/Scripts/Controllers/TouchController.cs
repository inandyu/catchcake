﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class TouchController : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{

    public float smoothing;
    public float sensetive = 1;
    
    private Vector2 origin;
    private Vector2 direction;
    private Vector2 smoothDirection;
    private bool touched;
    private int pointerID;

    public float MaxStrafe = 1;

    [Range(0,100)]
    public float MaxIntervalPercent = 10; //люфт
    float MaxInterval;
    public float MinForward = 0.5f;
    public float MaxForward = 1f;
    void Awake()
    {
        direction = Vector2.zero;
        touched = false;
        MaxInterval = (Screen.width/2) * ( MaxIntervalPercent/100);
       // Debug.Log(MaxInterval);
    }

    public void OnPointerDown(PointerEventData data)
    {
        Debug.Log("OnPointerDown");
        if (!touched)
        {
            touched = true;
            pointerID = data.pointerId;            
            origin = data.position;
            direction.x = 0;
            direction.y = 1;
        }
    }

    public void OnDrag(PointerEventData data)
    {
        if (data.pointerId == pointerID)
        {
            Vector2 currentPosition = data.position;
            Vector2 directionRaw = currentPosition - origin;    
            
            direction.x = directionRaw.x / MaxInterval;
            direction.x = Mathf.Min(direction.x, MaxStrafe);
            direction.x = Mathf.Max(direction.x, -MaxStrafe);

            direction.y = (directionRaw.y / MaxInterval); 
            direction.y = Mathf.Max(direction.y, MinForward);
            direction.y = Mathf.Min(direction.y, MaxForward ); // при -1  должно 0,5. при 0 = 1 и при 1 равно 1            
            //Debug.Log("direction"+ direction);
            
        }
    }
   
    public void OnPointerUp(PointerEventData data)
    {
       
        if (data.pointerId == pointerID)
        {
            
            direction = Vector3.zero;
            touched = false;
        }
    }

    public Vector2 GetDirection()
    {
       smoothDirection = Vector2.MoveTowards(smoothDirection, direction, smoothing);

       

        return smoothDirection * sensetive;
    }

    public bool GetTouch()
    {
        
        return touched;
    }

    public Vector2 GetPosition()
    {
        Vector2 vec = new Vector2();

        if(touched)
        {
            vec = origin;
        }
        return origin;
    }

    public void Update()
    {
       // touched = Input.GetMouseButton(0);
    }
}