﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class CatchCakeSceneController : MonoBehaviour {

    public Canvas MenuCanvas;
    public static CatchCakeSceneController instance;
    Camera cam;
    GameObject[] balls;
    public TouchController charTouchController;
    public TouchController handTouchController;
    public PlayerController player;
    public float timerMax;
    public float timeLeft { get;  protected set; }
  
    public GameObject gameOverText;

    

    public GameObject MenuScreen;
    public Button startButton, restartButton;
    public MouthController mouthController;
    public int catchScore { get; protected set; }
    public int lostScore { get; protected set; }
    public int eatScore { get; protected set; }



    private float maxWidth;
    public bool counting = false;
    bool isPlaying;

    
    
    public GameObject nextBall { get; protected set; }
    public Vector3 nextSpawnPosition { get; protected set; }

   

    CatchCakeUIPauseMenuController pauseMenu;
    CatchCakeUIPlayMenuController playMenu;

    CatchCakeUIGameoverMenuController gameoverMenu;

    // Use this for initialization
    void OnEnable()
    {    
        if(instance == null)
        {
            instance = this;
        }
       
        //startButton.onClick.AddListener(StartGame);
        if (cam == null)
        {
            cam = Camera.main;
        }
        balls = Resources.LoadAll<GameObject>("Cakes");
        player.Initialization(this);
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0.0f);
        Vector3 targetWidth = cam.ScreenToWorldPoint(upperCorner);
        float ballWidth = balls[0].GetComponent<Renderer>().bounds.extents.x;
        maxWidth = targetWidth.x - ballWidth;

        InitUI();
       
        counting = false;
        timeLeft = timerMax;
        StartGame();
    }


    void InitUI()
    {
        pauseMenu = MenuCanvas.GetComponentInChildren<CatchCakeUIPauseMenuController>(true) ;
        pauseMenu.Initialization();

        playMenu = MenuCanvas.GetComponentInChildren<CatchCakeUIPlayMenuController>(true);
        playMenu.Open();

        gameoverMenu = MenuCanvas.GetComponentInChildren<CatchCakeUIGameoverMenuController>(true);
        gameoverMenu.Close();
    }

    void FixedUpdate()
    {
        if (counting)
        {
            timeLeft -= Time.fixedDeltaTime;
            if (timeLeft < 0)
            {
                timeLeft = 0;
                counting = false;
            }            
        }
      
    }

    

    public void StartGame()
    {
        playMenu.Open();
        pauseMenu.Close();
        gameoverMenu.Close();

        MenuScreen.SetActive(false);
        //startButton.SetActive(false);
       // SaveSystemManager.AddGamesPlayedPlayerData(1);
        mouthController.ToggleControl(true);
        counting = true;
        StartCoroutine(Spawn());
    }

    public IEnumerator Spawn()
    {
        yield return new WaitForSeconds(1.0f);
        
         
        nextBall = balls[UnityEngine.Random.Range(0, balls.Length)];
        nextSpawnPosition = new Vector3(
            transform.position.x + UnityEngine.Random.Range(-maxWidth, maxWidth),
            transform.position.y,
            0.0f
        );

      

        while (counting)
        {
            
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(nextBall, nextSpawnPosition, spawnRotation,transform);

            nextBall = balls[UnityEngine.Random.Range(0, balls.Length)];
            nextSpawnPosition = new Vector3(
                transform.position.x + UnityEngine.Random.Range(-maxWidth, maxWidth),
                transform.position.y,
                0.0f
            );
            

            yield return new WaitForSeconds(UnityEngine.Random.Range(1.0f, 2.0f));

           
        }
        // yield return new WaitForSeconds(2.0f);
        //gameOverText.SetActive(true);
        // yield return new WaitForSeconds(2.0f);
        //restartButton.SetActive(true);
        TimeIsOver();
    }
    
    public void TakeEatScore(int value)
    {
        eatScore += value;
    }

    public void TakeCatchScore(int value)
    {
        eatScore += value;
    }

    public void TakeLostScore(int value)
    {
        eatScore += value;
    }

    public void TakeTime(float time)
    {
        timeLeft += time;
    }

    public void TakeCake(Cake cake, Cake.ScoreGetType scoreGetType)
    {
        switch (scoreGetType)
        {
            case Cake.ScoreGetType.EatScoreType:
                eatScore += cake.m_EatScore.ScoreValue;
                timeLeft += cake.m_EatScore.GetTime();
                Debug.Log("EatScoreType" + cake.m_EatScore);
                break;
            case Cake.ScoreGetType.CatchScoreType:
                catchScore += cake.m_CatchScore.ScoreValue;
                timeLeft += cake.m_CatchScore.GetTime();
                Debug.Log("CatchScoreType" + cake.m_CatchScore);
                break;
            case Cake.ScoreGetType.LostScoreType:
                lostScore += cake.m_LostScore.ScoreValue;
                timeLeft += cake.m_LostScore.GetTime();
                Debug.Log("LostScoreType" + cake.m_LostScore);
                break;
            case Cake.ScoreGetType.CoinScoreType:
                GameManager.instance.gameScore.AddCoin( cake.m_EatScore.ScoreValue);
                break;
            default:
                break;
        }
    }


    void TimeIsOver()
    {
        gameoverMenu.Open();

       // GameManager.instance.gameScore.CheckCatchScore(catchScore);
        //GameManager.instance.gameScore.CheckEatScore(eatScore);
    }

    void GameOver()
    {

    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        playMenu.Close();
        pauseMenu.Open();
    }

    internal void ContinueGame()
    {
        Time.timeScale = 1;

        playMenu.Open();
        pauseMenu.Close();
    }

    internal void ContinueGame(int v)
    {
        Time.timeScale = 1;
        timeLeft = v;
        
        StartGame();
    }

    internal void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
