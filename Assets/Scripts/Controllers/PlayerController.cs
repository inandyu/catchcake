﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject Hand;
    public GameObject Character;

    CatchCakeSceneController gameController;

	public void Initialization(CatchCakeSceneController _gameController)
    {
        gameController = _gameController;
    }

    private void FixedUpdate()
    {
        if (gameController.handTouchController.GetTouch())
        {
           

            //Debug.Log("handTouc direction " + gameController.handTouchController.GetDirection());
            var pos = Hand.transform.position;
            pos.y = Camera.main.ScreenToWorldPoint(gameController.handTouchController.GetPosition()).y;
           // Debug.Log("handTouc " + Camera.main.ScreenToWorldPoint(gameController.handTouchController.GetPosition()));
            Hand.transform.position = pos;


        }
    }
}
