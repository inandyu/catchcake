﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandController : MonoBehaviour {

    Transform m_parentTransoform;
    Animation anims;
    public GameObject m_Fist;
    
    public RectTransform m_MouthZoneControl;

    // Use this for initialization
    void Start () {
        anims = GetComponent<Animation>();
        m_parentTransoform = transform.parent.GetComponent<Transform>();
        //m_fist = transform.GetChild();
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKeyDown( KeyCode.A))
        {
            anims.clip = anims.GetClip("CatchLeft");
            anims.Play();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {

            anims.clip = anims.GetClip("CatchRight");
            anims.Play();
        }
        if(Input.GetMouseButtonDown(0))
        {


            //Debug.Log(Input.mousePosition.x);
           // m_parentTransoform.position = new Vector3(m_parentTransoform.position.x, turi());
            
            if (Input.mousePosition.x > Screen.width / 2)
            {
                CatchLeft();
            }
            else
            {
                CatchRight();
            }
            
        }
    }
    void CatchLeft()
    {
        anims.clip = anims.GetClip("CatchLeft");
        anims.Play();

    }
    void CatchRight()
    {
        anims.clip = anims.GetClip("CatchRight");
        anims.Play();
    }

    float turi()
    {

        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;
        
        Debug.DrawLine(camRay.origin, camRay.direction);
        Debug.DrawRay(camRay.origin, camRay.direction);
       
        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit))
        {
           
            return floorHit.point.y;
            
        }
        return camRay.origin.y;
    }

    public void DestroyCake()
    {

        if (m_Fist.transform.childCount != 0)
        {
            Cake cake = m_Fist.transform.GetChild(0).GetComponent<Cake>();
            if (cake)
            {
                cake.CatchCake();
            }

            
        }

    }
}
