﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploseEffectController : MonoBehaviour {

    float topPosY = -0.82f;
    float time = 0;
    public float timeToOff = 1;
    public int damageValue = -5;
    public static ExploseEffectController instance;

    SpriteRenderer sprite;

    public void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        sprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    


    public void StartExplose(Vector3 pos)
    {
        time = timeToOff;
        gameObject.SetActive(true);
        pos.y = topPosY;
        transform.position = pos;
        transform.localScale = Vector3.one;


        //SetPosition(CatchCakeSceneController.instance.nextSpawnPosition);
    }
    public void SetPosition(Vector3 pos)
    {
        pos.y = topPosY;
        transform.position = pos;
        Debug.Log("pos " + pos);

    }

    private void FixedUpdate()
    {
        if (time > 0)
        {
            time -= Time.fixedDeltaTime;
            transform.localScale = Vector3.one * (1 + (2- time/timeToOff));
        }
        else
        {
            gameObject.SetActive(false);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter explose");
        if(other.tag == "Player")
        {
            CatchCakeSceneController.instance.TakeTime(damageValue);
        }
    }
}
