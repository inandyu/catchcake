﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour {

    LocalizationManager locManager;
    // Use this for initialization
    void Start () {

        DontDestroyOnLoad(gameObject);
        locManager = GetComponent<LocalizationManager>();
        StartCoroutine(Loading());
       
    }
	
    IEnumerator Loading()
    {
        StartCoroutine(locManager.LoadLocalizedText());
        yield return null;
        while(locManager.GetIsReady() == false)
        {
            yield return null;
        }

        SceneManager.LoadScene("Level Scenes");
    }

	// Update is called once per frame
	void Update () {
		
	}
}
